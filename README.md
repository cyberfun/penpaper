
# History #

A friend came over to our office one day and started talking about the possibility
giving people a chance to encrypt messages without computers, just with a pen 
and paper. They would write a message, encrypt it by hand, burn/eat/melt the 
encryption tool (i.e., a sheet of paper), and send the message.


We thought it would be a really good fun to create something useful and so we,
eventually, created a procedure and some tools. It was quite easy from the 
cryptography point of view as we used a one-time pad - the only encryption 
scheme that is unconditionally secure (no-one can break it - with a small caveat). 
The real challenge was to create an encryption table that would make it easy for
anyone to encrypt and decrypt a message.

Due to our limitations to read small letters, our encryption sheets only allow
to encrypt 50 characters. Longer messages will need several encryption sheets.

# How To #

To send a secure message, you need a hardcopy of an encryption sheet - that's
what this repository is about.

However, like with any other security, the difference between the smart and silly
is how to use the tools you have.

Here's a "secure procedure" that ensures that noone will be ever able to decipher
encrypted messages.

## Basic Rules ##

1. you need a set of printed encryption sheets;
1. each sheet can be used only once. If you need to send 10 messages, you need 10 different sheets (unless you are just testing to see whether it works);
1. one sheet allows encryption of a message of up to 50 characters;
1. each sheet contains a code number that needs to be sent with the message - 5 letters that are in large font should be enough - so that the recipient can find the right decoding sheet; and
1. sheets MUST NOT be disclosed to a third party, as that would compromise the message.

## Basic Use ## 

1. print as many encryption sheets;
1. fill in the names of people who share an encryption sheet. (1) the person how keeps the decrypting part of the sheet, (2) who will be encrypting/sending.
1. cut the sheet into two - the upper part goes to the sender, the lower part goes to the recipient.

### Sending ###

1. write the message in the top row of empty boxes - maximum is 50 characters - skip spaces or replace them with 'X's;
1. for each character, find it in the column beneath it on the left, and write the character next to it into the box at the bottom of the column;
1. once you've done all the characters, destroy everything above the line "CUT HERE - DESTROY UPPER PART"; and
1. send the remaining piece to the recipient directly or type it into an SMS or email
1. You have to include the 5 letters of the sheet code - that will help the receiver find the decryption sheet.

### Receiving ###

Find the sheet with the code as received with a message.
Write the received message in the top row of boxes;
For each box, find its letter in the left column beneath and write the character next to it in the box at the bottom of the column;
You can read the message now (spaces are missing or replaced with 'X's). You should now destroy the whole sheet.

## How to Use Sheets - Professional ##

The 'professional' means that there is more than 2 people and you use this to send
messages between any two of them. You now have to create your key management / sheet management
so that encryption sheets don't get compromised.

* 1. Make a list of people who want to encrypt messages with our sheets and note who will want to talk to who.
* 1. Estimate how many messages each pair will send in a month (or other suitable time interval) and write it all down.

Note: each message requires one sheet and it is a good idea to have a small stock of sheets (too large a stock increases the danger of someone else making a copy and decrypting your messages).

* If you use a webbrowser to print sheets - switch your web browser to privacy mode so that no copies of sheets remains on your computer.
* Print as many sheets as you need and close the browser when finished.
* On each sheet, name who will keep the decrypting part of the sheet (1) and who will be encrypting (2).
* Write the date after which the sheet should not be used (0).
* Cut the sheet into two - the upper part goes to the sender, the lower part goes to the receiver (as you've just written on the sheet). Ideally, all steps up to now happen when senders and receivers are present.

### Sending ###


1. Write the message in the top row of empty boxes - maximum is 50 characters - skip spaces or replace them with 'X's; 
1. For each character, find it in the column beneath it on the left, and write the character next to it into the box at the bottom of the column;
1. Destroy everything above the line "CUT HERE - DESTROY UPPER PART"; and
1. Send the remaining piece to the recipient directly or type it into an SMS or email together with 5 letters of the sheet code.

### Receiving ###

1. Find the sheet with the sheet code as received with the message.
1. Write received message in the top line of boxes;
1. For each box, find its letter in the left-hand column beneath it and write the character next to it in the box at the bottom;
1. When you've read the message (spaces are missing or replaced with 'X's) destroy the whole sheet.

## Compromise Response - "Disaster Recovery (DR) Plan" ##

1. If you have a suspicion that some sheets have been compromised, senders and recipients must destroy all of their unused copies.

### Management of sheets ###

1. Define maximum time for keeping printed sheets and destroy them if you do not use them by then.
1. You can define further rules for keeping sheets safe and for their destruction in particular circumstances.

# Security Caveat #

The unconditional security depends on true randomness of encryption sheets. At
the moment, this is ensured by the fact that the generation algorithm is on our 
server and we make sure that everything is erased when a new encryption sheet is 
downloaded.

This however can be improved by:

* using secure hardware, moving server abroad, ... Let us know if you are interested in a hardened version; or
* generate sheets in Javascript on your computer instead on our server - we may do a Javascript version if we feel idle.

